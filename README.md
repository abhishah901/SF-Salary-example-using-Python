# SF-Salary-example-using-Python
A quick exercise to show my pandas skills! 
I will be using the SF Salaries Dataset(https://www.kaggle.com/kaggle/sf-salaries) from Kaggle!
I've used the jupyter notebook to solve the exercise!
I've kept my errors that occurred while solving the exercise in separate cells.
